﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mirarAlMouse : MonoBehaviour
{
    [SerializeField]
    private Transform rotar;



    private void FixedUpdate()
    {
        Ray rayOrigin = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;

        if(Physics.Raycast(rayOrigin,out hitInfo))
        {
            if(hitInfo.collider !=null)
            {
                Vector3 direction = hitInfo.point - rotar.position;
                rotar.rotation = Quaternion.LookRotation(direction);
            }
        }
    }
}
