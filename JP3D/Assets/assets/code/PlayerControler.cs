﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControler : MonoBehaviour
{
    public float horizontalMove;
    public float verticalMove;
    public CharacterController player;
    private Vector3 playerInput;

    [Header("movimiento")]
    public float playerSpeed;
    private Vector3 movePlayer;
    public float gravity;
    public float fallVelocity;
    public float jumpForce;

    [Header("Camara")]
    public Camera mainCamera;
    private Vector3 camForward;
    private Vector3 camRight;

    [Header("disparo")]
    public GameObject disparo;
    public Transform arma;
   // public float cadencia;
    private float sigDisp;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        horizontalMove = Input.GetAxis("Horizontal");
        verticalMove = Input.GetAxis("Vertical");

        playerInput = new Vector3(horizontalMove, 0, verticalMove);
        playerInput = Vector3.ClampMagnitude(playerInput, 1);

        camDirection();

        movePlayer = playerInput.x * camRight + playerInput.z * camForward;

        movePlayer = movePlayer * playerSpeed;

        player.transform.LookAt(player.transform.position + movePlayer);

        SetGravity();

        PlayerSkills();

        player.Move(movePlayer * Time.deltaTime);



        if (Input.GetKey(KeyCode.LeftShift))
        {
            playerSpeed = 22;
        }
        else
        {
            playerSpeed = 12;
        }
        //Debug.Log(player.velocity.magnitude);

    }
    //determina la dereccion en quemira la camara
    void camDirection()
    {
        camForward = mainCamera.transform.forward;
        camRight = mainCamera.transform.right;

        camForward.y = 0;
        camRight.y = 0;


        camForward = camForward.normalized;
        camRight = camRight.normalized;
    }
    //gravedad del jugador
    void SetGravity()
    {
        if (player.isGrounded)
        {
            fallVelocity = -gravity * Time.deltaTime*2;
            movePlayer.y = fallVelocity;
        }
        else
        {
            fallVelocity -= gravity * Time.deltaTime*2;
            movePlayer.y = fallVelocity;
        }
    }

    //habilidades del player
    void PlayerSkills()
    {
        if(player.isGrounded && Input.GetButtonDown("Jump"))
        {
            fallVelocity = jumpForce;
            movePlayer.y = fallVelocity;
        }
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
        
            Instantiate(disparo, arma.position, arma.rotation);
        }

       // if (Input.GetButton("Fire1") && Time.time > sigDisp)
       // {
       //     sigDisp = Time.time + cadencia;
       //     Instantiate(disparo, arma.position, arma.rotation);
       // }
    }
   void OnTriggerEnter(Collider other)
   {
       if(other.gameObject.tag=="lose")
       {
           SceneManager.LoadScene("lose");
       }
       if(other.gameObject.tag == "win")
       {
           SceneManager.LoadScene("win");
       }
    }
    void OnCollisionEnter(Collision collision)
    {
       if(collision.gameObject.tag=="win")
        {
            SceneManager.LoadScene("win");
        }
    }
}
