﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camara : MonoBehaviour
{
    private const float Y_ANGULE_MIN = -40.0f;
    private const float X_ANGULE_MIN = 90.0f; 

    public Transform lookAt;
    public Transform camTransform;

    private Camera cam;

    private float distance = 10.0f;
    private float currentX = 0.0f;
    private float currentY = 0.0f;
   // private float sensitivityX = 3.0f;
   // private float sensitivityY = 1.0f;

    // Start is called before the first frame update
    private void Start()
    {
        camTransform = transform;
        cam = Camera.main;


    }

    // Update is called once per frame
    private void LateUpdate()
    {
        Vector3 dir = new Vector3(0, 0, -distance);
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
        camTransform.position = lookAt.position + rotation * dir;
        camTransform.LookAt(lookAt.position);
    }

    private void Update()
    {
        currentX += Input.GetAxis("Mouse X");
        currentY += Input.GetAxis("Mouse Y");

        currentY = Mathf.Clamp(currentY, Y_ANGULE_MIN, X_ANGULE_MIN);

    }
}
