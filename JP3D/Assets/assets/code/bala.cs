﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bala : MonoBehaviour
{
    public float velocidad;
    private Rigidbody rig;


    private void Awake()
    {
        rig = GetComponent<Rigidbody>();
    }
    // Start is called before the first frame update
    void Start()
    {
        rig.velocity = transform.up * velocidad;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag=="objeto")
        {
            Destroy(collision.gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="objeto")
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
        if (other.gameObject.tag == "grounded")
        {
            Destroy(gameObject);
        }
        if (other.gameObject.tag == "lose")
        {
            Destroy(gameObject);
        }
        if (other.gameObject.tag == "grounded")
        {
            Destroy(gameObject);
        }
    }
}
